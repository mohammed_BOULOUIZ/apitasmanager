<?php
namespace App\Events;

use App\Entity\Task;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BindTaskToUser implements EventSubscriberInterface
{
    private $securityData;

    public function __construct(Security $security)
    {
        $this->securityData=$security;
    }

    public static function getSubscribedEvents()
    {
        // lorsque le KernetEvents arrive à l'evenement view et au moment de la validation des données
        // on inercepte l'evenement on incluant la methode  (ligne 36) qui permet d'ajouter un user à task
        return[
            KernelEvents::VIEW => ['setUserForTask', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function setUserForTask(ViewEvent $event)
    {
        $task = $event->getControllerResult();
        $methode = $event->getRequest()->getMethod();
        $user = $this->securityData->getUser();
        if ($task instanceof Task && $methode === "POST"){
             $task->setUser($user);
        }
       
    }
}
