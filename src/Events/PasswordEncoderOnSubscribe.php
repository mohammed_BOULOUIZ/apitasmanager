<?php

namespace App\Events;  

use App\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordEncoderOnSubscribe implements EventSubscriberInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;;
    }

/**
 * l'evenement à intercepter 
 */
    public static function getSubscribedEvents()
    {
        // lorsque le KernetEvents arrive à l'evenement view et qu'il arrive au moment d'ecrire les données en bdd
        // on inercepte l'evenement on incluant la methode encodPwd (ligne 36) qui permet d'hasher le mot de passe avant de l'ecrire
        return[
            KernelEvents::VIEW => ['encodPwd', EventPriorities::PRE_WRITE]
        ];
    }

/**
 * methode permettant d'encoder le mot de pass 
 */
    public function encodPwd(ViewEvent $event){
        //recuéprer les données de user via le controller
        $user = $event->getControllerResult();
        // recupérer la methode utiliser dans la requette, afin de s'assurer que par la suite on intervient sur une methdoe Post seulement (ligne 45)
        $method = $event->getRequest()->getMethod();// Post, Get, Put
        
        //si user est une instance de l'entity User et que la methode est poste 
        if ($user instanceof User && $method === "POST" ){
            // recupérer le mot de passe et l'encoder
            $hashedPW = $this->encoder->encodePassword($user, $user->getPassword());
            //remetre dans le $user le mot de passe encoder
            $user->setPassword($hashedPW);
        }elseif ($user instanceof User && $method==="PUT") {
            // recupérer le mot de passe et l'encoder
            $hashedPW = $this->encoder->encodePassword($user, $user->getPassword());
            //remetre dans le $user le mot de passe encoder
            $user->setPassword($hashedPW);
        }
    }
    
}


?>