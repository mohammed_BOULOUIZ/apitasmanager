<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 * @ApiResource(
 *      attributes={
 *          "normalization_context"={"groups"={"readTask"}},
 *          "denormalization_context"={"groups"={"writeTask"}}
 *      },
 *      collectionOperations={
 *          "post",
 *          "get",  
 *      },
 *      itemOperations={
 *          "get",
 *          "delete",
 *          "put",
 *      }
 * )
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"readTask"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"writeTask","readTask"})
     * @Assert\NotBlank(message = "le champs type de tache ne doit pas etre vide")
     * 
     */
    private $typeTask;

    /**
     * @ORM\Column(type="text")
     * @Groups({"writeTask","readTask"})
     * @Assert\NotBlank(message = "le champs description de la tache ne doit pas etre vide")
     */
    private $contentTask;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"writeTask","readTask"})
     * @Assert\NotBlank(message = "le datetime du systeme n'a pas été ajouté")
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"writeTask","readTask"})
     * @Assert\NotBlank(message = "une date de debut est obligatoire")
     * @Assert\LessThanOrEqual(propertyPath="stopAt", message="la date de debut est après la date de fin")
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"writeTask","readTask"})
     * Assert\GreaterThanOrEqual(propertyPath="startAt") ==> signifie que la date de fin doit etre egale
     * ou superieur à la date de debut
     * @Assert\GreaterThanOrEqual(propertyPath="startAt", message="la date de fin est avant la date de debut")
     */
    private $stopAt;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"readTask"})
     * @Assert\NotBlank(message = "session expirée veuillez vous connecter ! ou utilisateur introuvable")
     */
    private $user;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeTask(): ?string
    {
        return $this->typeTask;
    }

    public function setTypeTask(string $typeTask): self
    {
        $this->typeTask = $typeTask;

        return $this;
    }

    public function getContentTask(): ?string
    {
        return $this->contentTask;
    }

    public function setContentTask(string $contentTask): self
    {
        $this->contentTask = $contentTask;

        return $this;
    }

    public function getCreateAt():?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getStopAt(): ?\DateTimeInterface
    {
        return $this->stopAt;
    }

    public function setStopAt(?\DateTimeInterface $stopAt): self
    {
        $this->stopAt = $stopAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
