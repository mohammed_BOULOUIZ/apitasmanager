<?php
namespace App\Doctrine;

use App\Entity\Task;
use App\Entity\User;

use Symfony\Component\Security\Core\Security;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;

use Doctrine\ORM\QueryBuilder;


class TaskOfUserOnly implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->changeSqlRequest($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->changeSqlRequest($queryBuilder, $resourceClass);
    }

    //ajouter un where à la requete sql qui recupere toutes les task, afin qu'elle soit pointée vers un seul user et il doit etre connecté
    private function changeSqlRequest(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        // recupérer le user connecté
        $user = $this->security->getUser();

        // si on demande une task, donc agir sur la requete pour qu'elle tiennne compte des tasks liées à l'utilisateur...
        // ...connecté uniquement !!
        if ($resourceClass === Task::class && $user instanceof User){

        // recupérer l'alias qui permet de récrire la requet sql
        $rootAlias = $queryBuilder->getRootAliases()[0];

        // ajouter un where à la requete...
        // ..."$rootAlias.user= equivaut au nom de la colonne...
        // ...=:user" vaut le parametre de filtrage
        $queryBuilder->andWhere("$rootAlias.user = :user");

        // dans la methode setParametre on fournit le paramtre réel qui remplacera =:user"
        $queryBuilder->setParameter("user", $user);
        }




        //ORIGINAL APIPLATFORM QUERYBUILDER
        // if (Task::class !== $resourceClass || $this->security->isGranted('ROLE_ADMIN') || null === $user = $this->security->getUser()) {
        //     return;
        // }

        // $rootAlias = $queryBuilder->getRootAliases()[0];
        // $queryBuilder->andWhere(sprintf('%s.user = :current_user', $rootAlias));
        // $queryBuilder->setParameter('current_user', $user);
    }

}