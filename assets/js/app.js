
import React, { Fragment, useState, useEffect } from 'react';
import ReactDom from 'react-dom';
import { HashRouter, Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../css/app.css';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import TasksPage from './pages/TasksPage';
import TaskAPI from './services/TaskAPI';
import { set } from 'date-fns';

//à chaque actualisaiton de la page du naviagteur, cette fonction permet d'informer axios...
//...si il doit ou pas avoir un accés à l'api via un token
 TaskAPI.setup()



//le composant qui retourne toute l'application
const App = () => {
    const [isAuthenticated, setIsAuthenticated] = useState(TaskAPI.setup())
    const [notify, setNotify] = useState()
    const [lateTask, setLateTask] = useState()
    //NavbarWithrout est un composant qui retourne la Navbar comme si elle etais dans un coposant <rout .../>
    // et donc avec la fonction history
    const NavbarWithRout = withRouter(Navbar)
    //private Route: dans le cas d'une non authentifiaction ce composant permet de rediriger la navigation vers la page login...
    //...à reutiliser
    const PrivateRoute = ({ path, isAuthenticated, component: Component, setLateTask }) => {
        return <Route path={path}
            render={(props) => (
                isAuthenticated === true
                    ? <Component {...props} setLateTask={setLateTask} />
                    : <Redirect to="/Login" />
            )} />
    }
    useEffect(() => {
        setIsAuthenticated(TaskAPI.setup())
    }, [isAuthenticated])

    useEffect(() => {
        setNotify(lateTask)
    }, [lateTask])
   
    return (
        <Fragment>
            
            
            <HashRouter>
                {/* Le composant Navbar ne peut pas beneficier de l'objet HISTORY car elle n'est pas dans une route
            * de ce fait on utilise une fonction withRouter() que propose 'react-dom-route'
            * et en créer une <NavbarWithRout />
            */}
                <NavbarWithRout isAuthenticated={isAuthenticated} setIsAuthenticated={setIsAuthenticated} notify={notify} />
                <main>
                    <Switch>
                        <Route
                            path="/Login"
                            // render : permet de retourner un composant avec props et avec fonction
                            render={props => <LoginPage setIsAuthenticated={setIsAuthenticated} {...props} />}
                        />
                        <Route
                            path="/Register"
                            render={props => <RegisterPage  {...props} />}
                        />
                        <PrivateRoute
                            path="/Tasks"
                            component={TasksPage}
                            isAuthenticated={isAuthenticated}
                            setLateTask={setLateTask}
                        />
                        <Route
                            path="/"
                            component={HomePage}
                        />
                    </Switch>
                </main>
            </HashRouter>
            <ToastContainer position={toast.POSITION.TOP_CENTER} autoClose={6000} />
        </Fragment>
    )
}

const rootElement = document.querySelector('#app');
ReactDom.render(<App />, rootElement)