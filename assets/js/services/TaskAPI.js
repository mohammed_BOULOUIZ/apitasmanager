import axios from 'axios'
import paramTask from '../parametre/parametreTasks'
import paramUser from '../parametre/parametreUsers'
import jwtDecode from 'jwt-decode'
import cors from '../services/corsorigin'

function findAll() {
    return axios
        .get(paramTask.urlTaskAPI, {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
        .then(response => response.data['hydra:member'])
}
function deleteById(id) {
    return axios
        .delete(paramTask.urlTaskAPI + id, {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
}
function setTokenInAxios(token) {
    axios.defaults.headers["Authorization"] = "Bearer " + token
}
function checkLogin(userId) {
    return axios
        .post(paramUser.urlcheckLoginAPI, userId, {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
        .then(response => response.data.token)
        .then(token => {
            //stocker le token dans localStorage (stockage du navigateur)
            window.localStorage.setItem("authToken", token)
            //inclure le token dans le header d'axios, afin qu'axios emporter avec lui...
            //...le token("tant qu'il est valide") vers toutes les requetes necessitant une connexion
            setTokenInAxios(token)
        })
       
}
function logout() {
    window.localStorage.removeItem("authToken")
    delete axios.defaults.headers["Authorization"]
}
function setup() {
    //1- verifier l'existance d'un token
    const token = window.localStorage.getItem('authToken')
    //2- verifier si ce token est valide (la date de validation du token est recuperable par le decodage du token avec [nom insall jwt-decode] )
    if (token !== null) {
        const jwtData = jwtDecode(token)
        //si il y a un token on recupére sa date d'expiration en (s' :seconde)
        //on comparte date d'expiration du token avec la date de maintenant...
        //...(on multiplie token.exp par mille car new time() est en milliseconde...
        //...on ne divise pas new time(), pour evite les virgule)
        if (jwtData.exp * 1000 > new Date().getTime()) {
            //inclure le token dans le header d'axios, afin qu'axios emporter avec lui...
            //...le token("tant qu'il est valide") vers toutes les requetes necessitant une connexion
            axios.defaults.headers["Authorization"] = "Bearer " + token
            
            return true

        }
    } else {
        //si le token a expiré ou qu'il est inexistant
        //on supprime l'etat de le l'uthorization=> on utilisant logout car elle contient...
        //... les instrucitons necessaires qui permettent de se déloguer
        logout()
        return false
    }
}
function isAthuenticated() {
    setup()
    console.log(setup())
}
function addTask(objectJson) {
    return axios
        .post(paramTask.urlCreateTaskAPI, objectJson, cors.headers)
}
function getOneById(id) {
    return axios
        .get(paramTask.urlTaskAPI.concat(id), {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
}
function update(id, objectJson){
    return axios
        .put(paramTask.urlTaskAPI.concat(id), objectJson, {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
}
function addUser(objectJson){
    return axios
        .post(paramUser.urlCreatUserAPI, objectJson, {headers: {'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'}})
}



export default {
    findAll, deleteById, checkLogin, logout, setup, isAthuenticated, addTask, getOneById, update, addUser
}