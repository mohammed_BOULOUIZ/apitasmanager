import AppBar from '@material-ui/core/AppBar';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { fade, makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import MoreIcon from '@material-ui/icons/MoreVert';
import NotificationsIcon from '@material-ui/icons/Notifications';
import React, { useState, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import TaskAPI from '../services/TaskAPI';
import { toast } from 'react-toastify';


const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
 
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  NavLink: {
    color: 'white',
    
  }
}));

export default function Navbar({ isAuthenticated, setIsAuthenticated, history, notify }) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  {/** fonction qui permet d'ouvrire le menu dans l'icone profile*/ }
  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  {/** fonction qui permet de fermer le menu */ }
  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  {/** fonction qui permet d'ouvrire le menu en mode MOBILE*/ }
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  {/** fonction qui permet de fermer le menu en mode MOBILE*/ }
  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const LogOut = () => {
    TaskAPI.logout()
    setIsAuthenticated(false)
    toast.info("Vous etes déconnécté, à très bientôt :-)")
    history.push("/Login")

  }


  {/** css*/ }
  const menuId = 'primary-search-account-menu';
  {/** le composant qui regroupe toutes les actions des icons en mode PC*/ }
  const renderMenu = (
    !isAuthenticated &&
    //si on n'est pas connecté donc isAuthenticated = false on affiche ce fragement
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <NavLink to="/Login" >
        <MenuItem onClick={handleMenuClose}>Se connecter</MenuItem>
      </NavLink>
      <NavLink to="/Register" >
        <MenuItem onClick={handleMenuClose}>s'inscrire</MenuItem>
      </NavLink>
    </Menu>
    ||
    //si on est connecté donc isAuthenticated = false on affiche ce fragement
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
        <MenuItem onClick={LogOut}>Deconnexion</MenuItem>
    </Menu>
  );

  {/** css*/ }
  const mobileMenuId = 'primary-search-account-menu-mobile';
  {/** le composant qui regroupe toutes les actions des icons en mode MOBILE*/ }
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {isAuthenticated &&
        //si on est connecté donc isAuthenticated = true, donc on affiche ce fragement
        <MenuItem>
          <NavLink to="/Tasks"  >
            <IconButton aria-label="show {notify} new notifications" color="inherit">
              <Badge badgeContent={notify} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <p>Notifications</p>
          </NavLink>
        </MenuItem>}
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Compte</p>
      </MenuItem>
    </Menu>
  );

  console.log(isAuthenticated)
  return (
    <div className={classes.grow}>
      <AppBar position="static"> {/** debut de la Navbar */}
        <Toolbar>
          {/** le nom de l'application + button home*/}
          <NavLink to="/" className={classes.NavLink} >
            <IconButton color="inherit">
              <Badge>
                <FormatListNumberedIcon /> Accueil MaTODO
              </Badge>
            </IconButton> 
          </NavLink>
          <Typography className={classes.title} variant="h6" noWrap>
          </Typography>

          {/** css*/}
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {isAuthenticated &&
              <NavLink to="/Tasks" className={classes.NavLink} >
                <IconButton aria-label="show {notify} new notifications" color="inherit">
                  <Badge badgeContent={notify} color="secondary">
                    <NotificationsIcon />
                  </Badge>
                </IconButton>
              </NavLink>
            }

            {/** l'icone de compte/profile en mode PC' */}
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>{/** fin de la Navbar inculant le mode PC */}

          {/** l'icone/boutton qui regroupe toutes les icones en mode MOBILE */}
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>

      {/** le rendu du composant qui regroupe les actions en mode MOBILE */}
      {renderMobileMenu}

      {/** le rendu du composant qui regroupe les actions en mode PC */}
      {renderMenu}
    </div>
  );
}
