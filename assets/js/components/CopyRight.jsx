import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import React from 'react';

export default function CopyRight() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://google.com/">
          ma-todo.fr
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }