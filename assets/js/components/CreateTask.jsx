import DateFnsUtils from '@date-io/date-fns/';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Slide from '@material-ui/core/Slide';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import CancelIcon from '@material-ui/icons/Cancel';
import EditIcon from '@material-ui/icons/Edit';
import { DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Date } from 'core-js';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import TaskAPI from '../services/TaskAPI';
import RefreshIcon from '@material-ui/icons/Refresh';




const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const useStyles = makeStyles(theme => ({

    formControl: {
        display: 'inline-block',
        marginBottom: '20px',
        marginRight: '40px',
        marginTop: 15,
        minWidth: 120
    },

    Add: {
        display: 'inline-block',
        backgroundColor: 'green',
    },
    White:{
        color: 'white',
        marginLeft: '50px'
    }

}));


export default function CreateTask({ setIsUpDate, isUpdate, setIsCreated, id, setId, history }) {
    const classes = useStyles();
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth()) + "-" + current_datetime.getDate() + "T" + current_datetime.getHours() + ":" + current_datetime.getMinutes()
    const [task, setTask] = useState({typeTask: '', createAt: formatted_date, contentTask: '', startAt: '', stopAt: ''})
    const [strtAt, setStrtAt] = useState()
    const [stpAt, setStpAt] = useState()
    const [openDial, setOpenDial] = useState(false);

    
    // Fonction change pour gerer l'etat de la tache :
    // handleChaneStart (recupére et convertie une date de debut de la tache en string pour l'ecrire dans [Task])
    // handleChaneStop (recupére et convertie une date de debut de la tache en string pour l'ecrire dans [Task])
    // handleChange (recupére les données input (typeTask et contentTask) pour l'ecrire dans [Task])
    const handleChangeStart = (e) => {
        setStrtAt(e)
        setTask({ ...task, startAt: e.toISOString() })
    }
    const handleChangeStop = (e) => {
        setStpAt(e)
        setTask({ ...task, stopAt: e.toISOString() })
    }
    const handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setTask({ ...task, [name]: value})
    };
    const handleClickOpen = () => {
        setOpenDial(true);
    };
    const handleClickClose = (e) => {
        setOpenDial(false);
        cancel();
        toast.info("Modification abondonnée") ;
    };
    
    function reFresh() {
        cancel()
        }
    function cancel() {
        setIsUpDate(false)
        setIsCreated(false)
        setId()
        setTask({
            typeTask: '',
            createAt: formatted_date,
            contentTask: '',
            startAt: '',
            stopAt: ''
        })
        setStrtAt()
        setStpAt()
    }


    // pour poster les données vers l'API
    const submit = async (event) => {
        event.preventDefault()
        try {
            await TaskAPI.addTask(task)
            toast.success("Tache ajoutée avec succes")    
            cancel()
            setIsCreated(true)
            history.replace("/Tasks")        
        } catch (error) {
            if( task.stopAt==='' || task.startAt===''){
                toast.error("Dates invalides")
            }
            if (new Date(task.stopAt) < new Date(task.startAt)) {
                toast.error("Dates invalides")
            } 
            if (task.typeTask === '') {
                    toast.error("Le champ type est vide !!")
            } 
            if (task.contentTask === '') {
                toast.error("Le champ tache est vide !!")
            }
        }
        setOpenDial(false)
        reFresh()
    }

    // pour mettre à jours les données vers l'API
    const upDate = async (event) => {
        try {
            await TaskAPI.update(id, task)
            toast.success("Modification reussit, votre tache est mise à jour")
            cancel()       
        } catch (error) {
            if( task.stopAt==='' || task.startAt===''){
                toast.error("Dates invalides")
            }
            if (new Date(task.stopAt) < new Date(task.startAt)) {
                toast.error("Date de debut est posterieur à la date de fin")
            } 
            if (task.typeTask === '') {
                    toast.error("Le champ type est vide !!")
            } 
            if (task.contentTask === '') {
                toast.error("Le champ tache est vide !!")
            }
        }
        setOpenDial(false)
    }

    // pour recuperer une données
    const getUpdateTask = async (id) => {
        try {
            await TaskAPI.getOneById(id)
                .then(data => (
                    setTask({
                        typeTask: data.data.typeTask,
                        contentTask: data.data.contentTask
                    }),
                    setStrtAt(new Date(data.data.startAt)),
                    setStpAt(new Date(data.data.stopAt))
                ))
                
        } catch (error) {
            if (error['hydra:member'] = "Not Found") {
                toast.error("Modification impossible pour une tache inexistante, essayez d\'actualiser la page de votre navigateur")
            }
            toast.error("Erreur indéfinie, veuillez nous contacter : dev@mail.com")
        }
    }
    useEffect(() => {
        if (id !== undefined) {
            try {
                getUpdateTask(id)
                toast.info("Vos données sont chargées vous pouvez les modifier")
                toast.warn("La date de création n'est pas modifiable")
            }
            catch (erro) {
                toast.info(erro.message) 
            }
        }
    }, [id])



    return (
        <div>
            <form onSubmit={submit}>
                <FormControl className={classes.formControl} variant="outlined" >
                    <InputLabel className={classes.formControl} htmlFor="typeTask-native-required">Type</InputLabel>
                    <Select
                        className={classes.formControl}
                        onChange={handleChange}
                        label="Type"
                        name='typeTask'
                        value={task.typeTask ? task.typeTask : ""}
                        inputProps={{
                            name: 'typeTask',
                            id: 'typeTask-native-required',
                        }}
                        required={true}
                    >
                        <MenuItem aria-label="None" value="" />
                        <MenuItem value={'Professionnelle'}>Professionnelle</MenuItem>
                        <MenuItem value={'Personnelle'}>Personnelle</MenuItem>
                        <MenuItem value={'Administrative'}>Administrative</MenuItem>
                        <MenuItem value={'Sportive'}>Sportive</MenuItem>
                        <MenuItem value={'Bricolage'}>Bricolage</MenuItem>
                        <MenuItem value={'Associative'}>Associative</MenuItem>

                    </Select>

                    <TextField
                        className={classes.formControl}
                        id="outlined-multiline-flexible"
                        label="Descriptif de votre tache"
                        multiline
                        rowsMax="4"
                        name='contentTask'
                        value={task.contentTask}
                        onChange={handleChange}
                        variant="outlined"
                        required={true}

                    />
                    <MuiPickersUtilsProvider utils={DateFnsUtils} className={classes.formControl}>
                        <DateTimePicker
                            className={classes.formControl}
                            ampm={false}
                            label="Date Debut"
                            inputVariant="outlined"
                            value={strtAt ? strtAt : null}
                            onChange={handleChangeStart}
                            format="yyyy/MM/dd hh:mm"
                            allowKeyboardControl={true}
                            required={true}
 
                        />
                        <DateTimePicker
                            className={classes.formControl}
                            ampm={false}
                            label="Date Fin"
                            inputVariant="outlined"
                            value={stpAt ? stpAt : null}
                            onChange={handleChangeStop}
                            format="yyyy/MM/dd hh:mm"
                            allowKeyboardControl={true}
                            required={true}
                        />

                    </MuiPickersUtilsProvider>


                </FormControl>
            </form>
            {isUpdate
                ?
                <>
                    <Fab color="primary" aria-label="update" onClick={handleClickOpen} >
                        <EditIcon />
                    </Fab>
                    <Fab color="secondary" aria-label="cancel" onClick={handleClickClose} className={classes.upDate}>
                        <CancelIcon />
                    </Fab>
                </>
                :
                <Fab aria-label="add" onClick={handleClickOpen} className={classes.Add} >
                    <AddIcon />
                </Fab>
            }
            <Fab aria-label="add" onClick={reFresh} className={classes.White} >
                    <RefreshIcon />
                </Fab>
            

            {/* controlle Dialog */}
            <Dialog
                open={openDial}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClickClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Confirmation"}</DialogTitle>

                <DialogContent>

                    <DialogContentText id="alert-dialog-slide-description">
                       
                    </DialogContentText>

                </DialogContent>

                <DialogActions>

                    {isUpdate
                        ?
                        <>
                            <Button onClick={upDate} color="primary">Confirmer modification</Button>
                            <Button onClick={handleClickClose} color="primary">Annuler</Button>
                        </>
                        :
                        <>
                            <Button onClick={submit} color="primary">Confirmer</Button>
                            <Button onClick={handleClickClose} color="primary">Annuler</Button>
                        </>
                    }

                </DialogActions>

            </Dialog>
        </div >
    );
}
