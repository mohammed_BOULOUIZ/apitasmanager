const AP_URL = process.env.API_URL
const urlcheckLoginAPI = AP_URL + "login_check"
const urlCreatUserAPI = AP_URL + "users"
const urlUpDateUserAPI = AP_URL + "users/"
export default{
    urlcheckLoginAPI, urlCreatUserAPI, urlUpDateUserAPI
}