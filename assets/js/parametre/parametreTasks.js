
const AP_URL = process.env.API_URL
const urlTaskAPI = AP_URL + "tasks/"
const urlCreateTaskAPI = AP_URL + "tasks"

export default{
    urlTaskAPI, urlCreateTaskAPI
}