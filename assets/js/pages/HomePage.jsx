import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import Fab from '@material-ui/core/Fab';
import NavigationIcon from '@material-ui/icons/Navigation';
import React from 'react';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SaveIcon from '@material-ui/icons/Save';
var todoImages = require('../images/todoBackground.gif');


const useStyles = makeStyles((theme) => ({
  rootImage: {
    height: '100vh',

  },
  root: {
    minWidth: 450,
    height: 500,
    textAlign: 'center'

  },

  image: {
    backgroundImage: 'url(' + todoImages + ')',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  descipt: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(16),
      height: theme.spacing(16),
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    textAlign: 'center'
  },
  h3: {
    color: 'grey'
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

export default function HomePage({ setIsAuthenticated, history }) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;



  return (
    <Grid container component="main" className={classes.rootImage}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />

      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <div className={classes.descipt}>
            <Grid elevation={3}>
              <h1>Bienvenue sur <br />Ma-ToDo</h1>
            </Grid>
          </div>


          <Card className={classes.root}>
            <CardContent>
              <h3 className={classes.h3}>
                Dans ces temps de confinement <a href="https://www.gouvernement.fr/info-coronavirus" target="_blank" >difficile,</a><br />
                rien de mieux que de ce faire une TODO list pour faire <br />
                certains taches oubliées ou cumulées avec d'autres<br />
              </h3>
              <h1 className={classes.h1}>
                MA-ToDo, une douce application <br />
                vous accompagne dans cette demarche et vous aide à le faire
              </h1>
            </CardContent>

            <CardActions>
              <NavLink to="/Register">
                <Fab variant="extended">
                  <SaveIcon className={classes.extendedIcon} />S'inscrire
                </Fab>
              </NavLink>
            </CardActions>
            <CardActions>
              <NavLink to="/Login">
                <Fab variant="extended">
                  <ExitToAppIcon className={classes.extendedIcon} />Déja client? par ici
                </Fab>
              </NavLink>
            </CardActions>

          </Card>
        </div>
      </Grid>



    </Grid>
  );
}