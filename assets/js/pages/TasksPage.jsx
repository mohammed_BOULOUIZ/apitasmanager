import { green, red } from '@material-ui/core/colors';
import Fab from '@material-ui/core/Fab';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import QueryBuilderSharpIcon from '@material-ui/icons/QueryBuilderSharp';
import SnoozeSharpIcon from '@material-ui/icons/SnoozeSharp';
import moment from 'moment';
import React, { Fragment, useEffect, useState } from 'react';
import CreateTask from '../components/CreateTask';
import TaskAPI from '../services/TaskAPI';
import { toast } from 'react-toastify';
import TableLoader from '../components/TableLoader'
import WorkIcon from '@material-ui/icons/Work';
import HomeIcon from '@material-ui/icons/Home';
import RowingIcon from '@material-ui/icons/Rowing';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import BuildIcon from '@material-ui/icons/Build';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';

const useStyles = makeStyles(theme => ({
    table: {
        minWidth: 650,
    },
    margin: {
        margin: theme.spacing(0.7),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    greenColor: {
        color: green[500],
    },
    redColor: {
        color: red['A700'],
    },
    blackColor:{
        color: 'black'
    },
    root: {
        width: '100%',
        marginTop: 50
    },
    rootTxFld: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
            backgroundColor: red['A700']
        },
    },
    CreateTask: {
        marginBottom: '20px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    IconePro: {
        color: 'blue'
    },
    IconePerso: {
        color: 'red'
    },
    IconeAdmin: {
        color: 'black'
    },
    IconeSport: {
        color: 'purple'
    },
    IconeBrico: {
        color: 'orange'
    },
    IconeAsso: {
        color: 'green'
    },
}));






export default function TasksPage({ setLateTask, history }) {
    const classes = useStyles();
    const [tasks, setTasks] = useState([])
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth()) + "-" + current_datetime.getDate() + "T" + current_datetime.getHours() + ":" + current_datetime.getMinutes()
    const [taskRow, setTaskRow] = useState({typeTask: '', createAt: formatted_date, contentTask: '', startAt: '', stopAt: ''})
    const [idRow, setIdRow] = useState()    
    const [id, setId] = useState()
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(6)

    const [isUpdate, setIsUpDate] = useState(false)
    const [isCreated, setIsCreated] = useState(false)
    const [isLoading, setLoading] = useState(true)
    const [isChecked, setIsChecked] = useState(false)
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    function renderSwitch(typeTask) {
        switch (typeTask) {

            case 'Professionnelle':
                return <WorkIcon className={classes.IconePro} />;
            case 'Personnelle':
                return <HomeIcon className={classes.IconePerso} />;
            case 'Administrative':
                return <AttachFileIcon className={classes.IconeAdmin} />;
            case 'Sportive':
                return <RowingIcon className={classes.IconeSport} />;
            case 'Bricolage':
                return <BuildIcon className={classes.IconeBrico} />;
            case 'Associative':
                return <EmojiPeopleIcon className={classes.IconeAsso} />;
            default:
                return <foo />;
        }
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    const cancel = () => {
        setId()
    }

    // supprimer une tasks
    const Delete = async id => {

        const originalTasks = [...tasks] // un tableu contenant les données avant suppression et donc qui inclus l'element supprimer

        //1. l'approche optimiste
        setTasks(tasks.filter(tasks => tasks.id !== id))

        //2. l'aproche pessimiste
        try {
            await TaskAPI.deleteById(id)
            toast.success("Tache effacée")
        } catch (error) {
            setTasks(originalTasks) // si la suppression a foiré donc on remet à l'affichage l'ancien tableau et qui inclus l'element supprimer
            toast.error(error.message)
        }
    }
    //CETTE FONCITON PERMET D'EXECUTER UNE TACHE A INTERVAL RGULIER
    //DE CE FAIT, ELLE CALCULE TOUTE LES 2 MINUTES COMBIENT DE TACHES SONT EN RETARD
    //ET CELA EN COMPARANT LA DATE DE FIN AVEC CELLE D'AUJOURD'HUI
    const countLazyTask = (tasks) => {
        let count = 0
        tasks.map(task => {
            if (task.stopAt!== null && new Date(task.stopAt) < new Date()) {
                count = count + 1
            }
        })
        setLateTask(count)
    }
    // recupérer tous les tasks
    const FindAll = async () => {
        try {
            const data = await TaskAPI.findAll()
            setTasks(data)
            countLazyTask(data)
            setLoading(false)
        } catch (error) {
            toast.error('Aucune tache trouvée')

        }

    }
    const upDate = (id) => {
        setIsUpDate(true)
        setId(id)
    }

    function checked() {
        setIsChecked(true)
    } 


    const fetchChecked = () => {
            try {
                TaskAPI.update(idRow, taskRow)
                    toast.success("Felicitation ! vous avez achevé cette tache")
                    setIsChecked(false)     
            } catch (error) {
                console.log(error)
            }

    }

    //appel de la fonction FindAll   
    useEffect(() => {
        FindAll(); // on passe par une fonction dans le useEffect, car ce use ne tolere pas les fonciton Async
        if(isChecked===true){
            fetchChecked()
        }        

    }, [isUpdate, isCreated, isChecked ])


    return (

        <Fragment>

            <div className={classes.CreateTask}>
                <CreateTask setIsUpDate={setIsUpDate}  isUpdate={isUpdate} setIsCreated={setIsCreated} id={id} setId={setId} history={history} />
            </div>

            <Paper className={classes.root}>
                <TableContainer component={Paper}>
                    <Table className={classes.table} size="small" aria-label="a simple table">
                        <TableHead >
                            <TableRow>
                                <TableCell align="center">Action</TableCell>
                                <TableCell align="left">Type</TableCell>
                                <TableCell align="center">Date de creation</TableCell>
                                <TableCell align="center">Tache</TableCell>
                                <TableCell align="center">Debut</TableCell>
                                <TableCell align="center">Fin</TableCell>
                                <TableCell align="right">Statu</TableCell>
                            </TableRow>
                        </TableHead>

                        {/* Loader */}
                        {isLoading
                            ?
                            <TableLoader />
                            :
                            <TableBody>
                                {tasks.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(task => (
                                    <TableRow key={task.id}>
                                        <TableCell align="center" >
                                            <Fab size="small" color="primary" aria-label="edit" className={classes.margin} onClick={() => upDate(task.id)}>
                                                <EditIcon />
                                            </Fab>
                                            <Fab size="small" color="secondary" aria-label="edit" className={classes.margin} onClick={() => { (window.confirm('Are you sure you wish to delete this item?')) ? Delete(task.id) : cancel() }}>
                                                <DeleteForeverIcon />
                                            </Fab>

                                        </TableCell>

                                        <TableCell align="left" >{renderSwitch(task.typeTask)} {task.typeTask}</TableCell>                                  
                                        <TableCell align="center" type="date">{moment(task.createAt).format('D/MM/YYYY à h:mm')}</TableCell> 
                                        <TableCell align="center">{task.contentTask}</TableCell>
                                        <TableCell align="center" type="date">{moment(task.startAt).format('D/MM/YYYY à h:mm')}</TableCell>
                                        {(task.stopAt !== null) &&
                                        
                                        <TableCell align="center" type="date">{moment(task.stopAt).format('D/MM/YYYY à h:mm')}</TableCell>
                                        ||
                                        <TableCell align="center" ><AllInclusiveIcon /></TableCell>
                                        }
                                        {
                                            (task.stopAt !== null && new Date(task.stopAt) < new Date) &&

                                                <TableCell align="right" >
                                                    <Fab size="small" className={classes.margin} className={classes.redColor} name='id' value={task.id} onClick={() => (setTaskRow({...task, stopAt: null}), setIdRow(task.id), checked())}><SnoozeSharpIcon  /></Fab>
                                                </TableCell>
                                        }
                                        {
                                            (new Date(task.stopAt) > new Date) &&

                                                <TableCell align="right">
                                                    <Fab size="small" className={classes.margin} className={classes.greenColor} name='id' value={task.id} onClick={() => (setTaskRow({...task, stopAt: null}), setIdRow(task.id), checked())}><QueryBuilderSharpIcon /></Fab>
                                                </TableCell>
                                        }
                                        {
                                            (task.stopAt === null) &&

                                                <TableCell align="right">
                                                    <Fab size="small" className={classes.margin} className={classes.blackColor}><CheckCircleIcon /></Fab>
                                                </TableCell>
                                        }
                                                

                                        

                                    </TableRow>
                                ))}


                            </TableBody>
                        }

                    </Table>
                </TableContainer>

                <TablePagination
                    rowsPerPageOptions={[6, 12, 24, 48]}
                    labelRowsPerPage={'Taches par page'}
                    component="div"
                    count={tasks.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </Fragment>
    );
}

