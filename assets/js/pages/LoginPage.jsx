import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FilledInput from '@material-ui/core/FilledInput';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { toast } from 'react-toastify';
import Copyright from '../components/CopyRight';
import TaskAPI from '../services/TaskAPI';
var todoImages = require('../images/todoBackground.jpg');

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    
    image: {
        backgroundImage: 'url('+todoImages+')',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function LoginPage({ setIsAuthenticated, history }) {
    const classes = useStyles();
    const [credentials, setCredentials] = useState({ username: "", password: "" })
    const [err, setErr] = useState({
        errUserName: false,
        errPassword: false,
    })

    const handleClickShowPassword = () => {
        setCredentials({ ...credentials, showPassword: !credentials.showPassword });
    };

    const handleMouseDownPassword = event => {
        event.preventDefault();
    };

    const onChangeValues = (event) => {
        const value = event.currentTarget.value
        const name = event.currentTarget.name
        setCredentials({ ...credentials, [name]: value })
    }

    const submitLogin = async (event) => {
        event.preventDefault();
        try {
            //recupérer le token depuis l'api via l'url ^api/login_check
            await TaskAPI.checkLogin(credentials)
            setIsAuthenticated(true);
            toast.success("Connexion réussit, bienvenue dans votre liste des taches")
            .then(history.replace("/Tasks"))
        } catch (error) {
            if(error.message === "Request failed with status code 401"){
                toast.error("Echec connexion : nom d'utilisateur et/ou mot de passe introuvable")
                setErr({errUserName : true, errPassword : true})
            }
        }

    }

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Connexion
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={submitLogin}>
                        <TextField
                        className={classes.form}
                            id="standard-helperText"
                            label="Nom d'utilisateur"
                            helperText="Some important text"
                            type={'text'}
                            name='username'
                            value={credentials.username}
                            onChange={onChangeValues}
                            required={true}
                            error={err.errUserName}
                        />
                        <InputLabel className={classes.form} htmlFor="filled-adornment-password">Mot de passe</InputLabel>
                        <FilledInput
                        className={classes.form}
                            id="filled-adornment-password"
                            type={credentials.showPassword ? 'text' : 'password'}
                            name='password'
                            value={credentials.password}
                            onChange={onChangeValues}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {credentials.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            required={true}
                            error={err.errPassword}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                        <Grid container>
                            <Grid item>
                                <NavLink to="/Register" variant="body2">
                                    {"Pas encore inscrit? par ici"}
                                </NavLink>
                            </Grid>
                        </Grid>
                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}