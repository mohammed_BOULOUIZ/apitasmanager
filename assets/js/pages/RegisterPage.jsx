import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import CopyRight from '../components/CopyRight';
import TaskAPI from '../services/TaskAPI';
import { toast } from 'react-toastify';


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: 10,
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function RegisterPage({ history }) {
    const classes = useStyles();
    const [credentials, setCredentials] = useState({ name: '', username: '', password: '', confirmPassword: '' });

    const onChangeValues = (event) => {
        const value = event.currentTarget.value
        const name = event.currentTarget.name
        setCredentials({ ...credentials, [name]: value.trim() })
    }


    const submitRegister = async (event) => {
       
        if (credentials.password >= 8 && credentials.password === credentials.confirmPassword) {
            try {
                await TaskAPI.addUser(credentials)
                history.replace("/Login")
                toast.success('Votre compte a été crée avec sucss')              

            } catch (error) {
 
                const value= true
                if (credentials.name === '') {
                    toast.error("Le nom est obligatoire")
                }
                if (credentials.username === '') {
                    toast.error("L'identifiant de connexion est obligatoire")

                }
                if (credentials.password === ''){
                    toast.error('les mots de passe est obligatoire')
                }
                if (credentials.password !== credentials.confirmPassword){
                    toast.error('les mots de passe ne sont pas identiques')
                }
                if ((credentials.password.length < 8) || (credentials.password.length > 15)){
                    toast.error('Le mot de passe doit faire entre 8 et 15 caracteres')
                }
            }    
        }  
        
        
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
          </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="name"
                                label="Votre nom"
                                name="name"
                                value={credentials.name}
                                onChange={onChangeValues}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="username"
                                label="votre nom d'utilisateur"
                                name="username"
                                value={credentials.username}
                                onChange={onChangeValues}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                value={credentials.password}
                                label="Password"
                                type="password"
                                id="password"
                                onChange={onChangeValues}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="confirmPassword"
                                value={credentials.confirmPassword}
                                label="confirmez votre mot de Password"
                                type="password"
                                id="confirmPassword"
                                onChange={onChangeValues}
                            />
                            <p>le mot de passe doit faire entre 8 et 15 caractères</p>
                        </Grid>
                    </Grid>
                    <Button
                        onClick={submitRegister}
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    > S'inscrire</Button>

                    <Grid container justify="center">
                        <Grid item>
                            <NavLink to='/Login' variant="body2">
                                Vous avez déja un compte? identifiez vous par ici
                </NavLink>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <CopyRight />
            </Box>
        </Container>
    );
}